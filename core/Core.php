<?php

namespace core;

class Core
{ 
    public static $instance;
    public  $mainTemplate;
    private  $DB;
    private function __construct()
    {

    }
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new Core();
            return self::getInstance();
        } else {
            return self::$instance;
        }
    }
    public function init()
    {
        session_start();
        spl_autoload_register('\core\Core::__autoload');
        $this->mainTemplate = new Template(); 
    }

    public function run()
    {
        $route = $_GET['path'];
        $pathParts = explode('/', $route);
        $className = ucfirst($pathParts[0]);
        if (empty($className)) {
            $fullClassName = 'controllers\\Site';
        } else
            $fullClassName = 'controllers\\' . $className;
        $methodName = ucfirst($pathParts[1]);
        if (empty($methodName)) {
            $fullMethodName = 'actionIndex';
        } else
            $fullMethodName = 'action' .$methodName;
        if (class_exists($fullClassName)) {
            $controller = new $fullClassName();
            if (method_exists($controller, $fullMethodName)) {
                $method = new \ReflectionMethod($fullClassName, $fullMethodName);
                $paramsArray = [];
                foreach ($method->getParameters() as $parameter) {
                    array_push($paramsArray, isset($_GET[$parameter->name]) ? $_GET[$parameter->name] : null);
                }
                $result = $method->invokeArgs($controller, $paramsArray);
                if (is_array($result)) {
                    $this->mainTemplate->setParams($result);
                }
            } else
                throw new \Exception('404 Сторінку не знайдено');
        } else {
            throw new \Exception('404 Сторінку не знайдено');
        }
    }
    public function done()
    {
       $this->mainTemplate->display('views/layout/index.php');
    }
 
    public static function __autoload($className)
    {
        $fileName = $className . '.php';
        if (is_file($fileName)) {
            include($fileName);
        }
    }

  
}